var btnContainer = document.getElementById("locationdiv");

    
    var btns = btnContainer.getElementsByClassName("location-container");
    
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");

    
       
        if (current.length > 0) {
          current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
      });
    } 

    var sidebar = new Vue({
      el: '#app',
      data: {
        data: [],
        viewedData: [],
        search: null,
        isSearched: false,
        map: null,
        markers: []
      },
      methods:{
        loadData(){
          let self = this
          axios.get('https://secure-badlands-41449.herokuapp.com/restaurants', { 
            headers: {'Access-Control-Allow-Origin': '*'},
            crossdomain: true,
          })
          .then(function (response) {
            //  self.data.push(response.data.restaurants[0])
            self.data = response.data.restaurants
            self.viewedData = self.data
            self.myMap();
          })
          .catch(function (error) {
            console.log(error);
          });
        },
        searchEnter(){
          if(this.search && this.search.length <= 5){
            this.search = this.search.trim()
            this.isSearched = true
            if(47 < this.search.charCodeAt(this.search.length - 1) && this.search.charCodeAt(this.search.length - 1) < 57){
              this.viewedData = []
              this.data.forEach(element => {
                if(element.zip.startsWith(this.search)){
                  this.viewedData.push(element);
                }
              });
            }
            else{
              this.search = this.search.substring(0,this.search.length - 1)
            }
          }
          else if(this.search.length > 5){
            this.search = this.search.substring(0,5)
          }
        },
        viewAll(){
          this.search = null
          this.viewedData = this.data
          this.isSearched = false
        },
        myMap(){
          self = this
          var mapProp = {
            center: new google.maps.LatLng(this.data[0].latitude, this.data[0].longitude),
            zoom: 14,
          };
          this.map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
          this.markers = this.data.map(function(element) {
            return new google.maps.Marker({
              position: {lat: element.latitude, lng: element.longitude},
              map: self.map,
              icon: '/images/pin.png'
            });
          });

          this.markers.forEach(marker => {
            marker.addListener('click', function() {
              console.log(marker)
              // let infowindow = new google.maps.InfoWindow({
              //   content: contentString
              // });
              // infowindow.open(map, marker);
            });
          })
        },
        zoomMarker(restaurant){
          let self = this
          var center = new google.maps.LatLng(restaurant.latitude, restaurant.longitude);
          // using global variable:
          this.map.panTo(center);
          this.markers.forEach(marker => {
            if(restaurant.latitude == marker.getPosition().lat() || restaurant.longitude == marker.getPosition().lng()){
              marker.setIcon('/images/pinactive.png')
            }
            else{
              marker.setIcon('/images/pin.png')
            }
          })
        }
      },
      created: function () {
       this.loadData();
      }
    })
var $clicked = true;
$('#btnmap').click(function(){

   $clicked = !$clicked;
   if(!$clicked){
    $('.location-container').hide();
    $('.location-container').removeClass('d-flex');
    $('.container-maps').css('display','block');
    $('.mapbtn').html('<i class="far fa-list-ul"></i><span>List</span>');
   }
   if($clicked)
   {
    $('.location-container').show();
    $('.location-container').addClass('d-flex');
    $('.container-maps').css('display','none');
    $('.mapbtn').html('<i class="fal fa-globe-americas"></i><span> Map</span>');

   }
});


//     console.log('click');
//     console.log($clicked)


// });
// new Vue({
// el: '#app',

// });
